<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;
use yii\helpers\Json;

/**
 * ProductSearch represents the model behind the search form about `common\models\Product`.
 */
class ProductSearch extends Product
{
    public $category;
    public $price;
    public $chains;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amount'], 'integer'],
            [['price'], 'number'],
            [['category', 'photo'], 'string'],
            [['name', 'barcode', 'description', 'unit', 'category', 'chains'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = Product::find()->joinWith(['prices' => function($query){
            $query->orderBy('currentPrice');
        }])->groupBy('id');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_ASC],
                'attributes' => [
                    'id',
                    'name',
                    'barcode',
                    'amount',
                    'price' => [
                        'asc' => ['MIN(price.currentPrice)' => SORT_ASC],
                        'desc' => ['MIN(price.currentPrice)' => SORT_DESC],
                    ],
                ],
            ]
        ]);



        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'product.id' => $this->id,
            'amount' => $this->amount,
            'price.currentPrice' => $this->price
        ]);

        if (!empty($this->category)) {
            $query->joinWith('categories');
            if (is_numeric($this->category)) $query->andFilterWhere(['category.id' => (int)($this->category)]);
            else $query->andFilterWhere(['like', 'category.name', $this->category]);
        }


        $query->andFilterWhere(['like', 'product.name', $this->name])
            ->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'unit', $this->unit]);

        return $dataProvider;
    }
}
