<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FormatConverter;

/**
 * This is the model class for table "price".
 *
 * @property integer $product_id
 * @property integer $chain_id
 * @property string $basePrice
 * @property string $currentPrice
 * @property string $comment
 * @property string $sale_before
 * @property integer $updated_at
 *
 * @property Chain $chain
 * @property Product $product
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'chain_id', 'currentPrice'], 'required'],
            [['product_id', 'chain_id'], 'integer'],
            [['basePrice', 'currentPrice'], 'number'],
            [['comment'], 'string', 'max' => 255],
            [['sale_before'], 'date', 'type' => 'date', 'format' => 'Y-m-d']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'chain_id' => 'Chain ID',
            'basePrice' => 'Base Price',
            'currentPrice' => 'Current Price',
            'comment' => 'Comment',
            'sale_before' => 'Sale before',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        if (empty($this->basePrice))
            $this->basePrice = $this->currentPrice;

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChain()
    {
        return $this->hasOne(Chain::className(), ['id' => 'chain_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
