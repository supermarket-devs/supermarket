<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use common\models\Barcode;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $amount
 * @property string $unit
 * @property string $photo
 *
 * @property Price[] $prices
 * @property Barcode[] $productBarcodes
 * @property Chain[] $chains
 * @property Category[] $categories
 * @property Tag[] $tags
 */
class Product extends \yii\db\ActiveRecord
{

    var $barcodes = null;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['amount'], 'number'],
            [['name', 'photo'], 'string', 'max' => 255],
            [['unit'], 'string', 'max' => 5],
            [['barcodes'], 'each', 'rule' => ['string', 'max' => 32]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'amount' => 'Amount',
            'unit' => 'Unit',
            'photo' => 'Photo'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(Price::className(), ['product_id' => 'id'])->orderBy('currentPrice');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChains()
    {
        return $this->hasMany(Chain::className(), ['id' => 'chain_id'])->viaTable('price', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('product_category', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('product_tag', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductBarcodes()
    {
        return $this->hasMany(Barcode::className(), ['product_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Barcode::deleteAll(['product_id' => $this->id]);
        if ($this->barcodes != null)
        {
            foreach ($this->barcodes as $barcode)
            {
                $model = Barcode::findOne(['barcode' => $barcode]);
                if ($model == null)
                {
                    $model = new Barcode();
                    $model->barcode = $barcode;
                    $model->save();
                }
                if ($model != null)
                    $this->link('productBarcodes', $model);
            }
        }
    }

    function fields(){
        return ['id', 'name',
                'barcodes' => function($model){
                    return ArrayHelper::getColumn($model->productBarcodes, 'barcode', false);
                },
                'description', 'amount', 'unit', 'prices', 'photo'];
    }

    function extraFields(){
        return [];
    }
}
