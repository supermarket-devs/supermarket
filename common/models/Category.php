<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $level
 * @property string $name
 * @property string $url
 * @property integer $parent
 *
 * @property TagGroup[] $tagGroups
 * @properties Category[] $children
 * @property Product[] $products
 */
class Category extends \yii\db\ActiveRecord
{
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parent', 'level'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent' => 'Parent',
            'url' => 'Url on novus.zakaz.ua'
        ];
    }

    public function beforeSave($insert){
        if ($this->parent > 0){
            $parent = Category::findOne($this->parent);
            if ($parent)
                $this->level = $parent->level+1;
        }
        return parent::beforeSave($insert);
    }

    public function afterDelete()
    {
        $cats = Category::find()->where(['parent' => $this->id])->all();
        foreach ($cats as $cat) {
            echo $cat->delete();
        }
        parent::afterDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagGroups()
    {
        return $this->hasMany(TagGroup::className(), ['id' => 'tag_group_id'])->viaTable('category_tag_group', ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('product_category', ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Category::className(), ['parent' => 'id'])->orderBy('name');
    }

    public function getParents()
    {
        $result = [];
        $parent = $this;
        while ($parent->parent > 0)
        {
            $parent = Category::findOne($parent->parent);
            if ($parent)
                array_unshift($result, $parent);
        }
        return $result;
    }

    public function getHierarchyAsArray($modifier = null, $parents = null){

        if ($modifier) $val = call_user_func($modifier, $this, $parents);
        else $val = $this;
        $result = [$val];
        if (!$parents) $parents = [$this];
        else $parents []= $this;
        foreach ($this->children as $child)
            $result = ArrayHelper::merge($result, $child->getHierarchyAsArray($modifier, $parents));

        return $result;
    }

    public function fields()
    {
        return ['id', 'name'];
    }

    public function extraFields()
    {
        return ['children', 'tagGroups', 'url', 'parent'];
    }
}
