<?php

namespace common\models;

use DateTime;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class ProductUpdater
{
    private static $instance;
    private $_chains;
    private $product;

    public static function instance(){
        if (!ProductUpdater::$instance)
            ProductUpdater::$instance = new ProductUpdater();
        return ProductUpdater::$instance;
    }

    private function __construct(){}

    private function get($data, $key, $default = null)
    {
        if (!empty($data[$key]))
            return $data[$key];
        return $default;
    }

    private function chain($name)
    {
        if (!$this->_chains) {
            $t = Chain::find()->asArray()->all();
            $this->_chains = [];
            foreach ($t as $c)
                $this->_chains[strtolower($c['name'])] = $c['id'];
        }

        return $this->get($this->_chains, strtolower($name));
    }

    public function updateCategoriesFromData($data)
    {
        try {
            $product = Product::find()->where(['barcode' => $this->get($data, 'barcode')])->one();
            if (!$product) return ['error' => 'Product not found'];
            $category = Category::find()->where(['url' => $this->get($data, 'category')])->one();
            if (!$category) return ['error' => 'Category not found'];
            $product->link('categories', $category);
            return ['message' => 'Success'];
        }catch (Exception $e){
            return ['error' => $e->getMessage()];
        }
    }


    function updateCategoriesFromJsonlinesFile($filename)
    {
        $file = fopen($filename, "r");
        if (!$file) return ["error" => "File can't be opened"];

        $products = Yii::$app->db->createCommand("SELECT product_id, barcode FROM barcode")->queryAll();
        $categories = Yii::$app->db->createCommand("SELECT category_id, alias FROM category")->queryAll();

        $products = ArrayHelper::map($products, 'barcode', 'product_id');
        $categories = ArrayHelper::map($categories, 'alias', 'category_id');

        $result = [];
        $line = fgets($file);
        $i = 0;

        $insert = [];
        while ($line){
            $data = Json::decode($line);

            if ($data['_type'] == 'ProductCategoryItem' && isset($products[$data['barcode']]) && isset($categories[$data['category']]))
                $insert []= [$products[$data['barcode']], $categories[$data['category']]];

            $i++;
            $line = fgets($file);
        }
        fclose($file);

        $tr = Yii::$app->db->beginTransaction();
        if (count($insert) > 0) {
            $tr = Yii::$app->db->beginTransaction();
            try {
                $sql = Yii::$app->db->createCommand()->batchInsert('product_category', ['product_id', 'category_id'], $insert)->sql;
                $sql = str_replace('INSERT', 'INSERT IGNORE', $sql);
                Yii::$app->db->createCommand($sql)->execute();
                $tr->commit();
            }
            catch (Exception $e) {
                $tr->rollBack();
                return ["error" => $e->getMessage(), 'SQL ERROR: updateCategoriesFromJsonlinesFile'];
            }
        }
        return $result;
    }

    public function addBarcodes($filename)
    {
        $file = fopen($filename, "r");
        if (!$file) return ["error" => "File can't be opened"];

        $products = Yii::$app->db->createCommand("SELECT product_id, barcode FROM barcode")->queryAll();
        $products = ArrayHelper::map($products, 'barcode', 'product_id');

        $line = fgets($file);
        $i = 0;
        $insert = [];
        while ($line){
            $data = Json::decode($line);

            if ($data['_type'] == 'BarcodeItem' && isset($products[$data['barcode']]))
                $insert []= [$products[$data['barcode']], $data['barcode_add']];

            $i++;
            $line = fgets($file);
        }
        fclose($file);


        if (count($insert) > 0) {
            $tr = Yii::$app->db->beginTransaction();
            try {
                $sql = Yii::$app->db->createCommand()->batchInsert('barcode', ['product_id', 'barcode'], $insert)->sql;
                $sql = str_replace('INSERT', 'INSERT IGNORE', $sql);
                Yii::$app->db->createCommand($sql)->execute();
                $tr->commit();
            } catch (Exception $e) {
                $tr->rollBack();
                return ["error" => $e->getMessage(), 'SQL ERROR: AddBarcodes'];
            }
        }


        return ['OK'];
    }

    public function addFromJson($filename)
    {
        $file = fopen($filename, "r");
        if (!$file) return ["error" => "File can't be opened"];

        $products = array_flip(Yii::$app->db->createCommand("SELECT barcode FROM barcode")->queryColumn());

        $line = fgets($file);

        $products_insert = [];

        while ($line){
            $data = Json::decode($line);

            $line = fgets($file);

            if ($data['_type'] == 'PriceItem')
                continue;

            if (!isset($products[$data['barcode']]))
            {
//                echo $data['barcode'] . "\n";
                if (isset($data['unit'])) {
                    $product = [$data['barcode'], $data['name'], $data['amount'], $data['unit'], $data['photo']];
                    $products_insert [] = $product;
                    $products[$data['barcode']] = 1;
                }
                else {
                    echo "Not found: [".var_dump($data)."]\n";
                }
            }
        }
        fclose($file);

        $tr = Yii::$app->db->beginTransaction();
        try
        {
            if (count($products_insert) > 0) {
                Yii::$app->db->createCommand()->batchInsert('product', ['tmp_key', 'name', 'amount', 'unit', 'photo'],
                    $products_insert)->execute();
                Yii::$app->db->createCommand('INSERT IGNORE INTO barcode (product_id, barcode) SELECT id, tmp_key FROM product WHERE NOT ISNULL(tmp_key)')->execute();
                Yii::$app->db->createCommand()->update('product', ['tmp_key' => null], 'NOT ISNULL(tmp_key)')->execute();
            }
            $tr->commit();
            echo count($products_insert) . " products added\n";
        }
        catch(Exception $e){
            $tr->rollBack();
            return ["error" => $e->getMessage()];
        }

        return ['OK'];
    }

    public function updateFromJsonlinesFileByBarcode($filename)
    {
        $m = memory_get_usage();
        $r = $this->addFromJson($filename);
        if (isset($r['error'])) {
            return $r;
        }

        $r = $this->addBarcodes($filename);
        if (isset($r['error'])) {
            return $r;
        }

        $file = fopen($filename, "r");

        $pr = Yii::$app->db->createCommand(
            'SELECT id, barcode.barcode, price.chain_id, price.basePrice, price.currentPrice, price.comment, price.updated_at, price.sale_before
              FROM product 
              LEFT JOIN price ON product.id = price.product_id
              LEFT JOIN barcode ON product.id = barcode.product_id')->queryAll();

        $products = [];
        $updated = [];
        foreach ($pr as &$p){
            if (!isset($products[$p['barcode']]))
                $products[$p['barcode']] = [$p];
            else
                $products[$p['barcode']] []= $p;
        }

        unset($pr);

        $line = fgets($file);

        $prices_insert = [];

        $today = time();
        while ($line){
            $data = Json::decode($line);

            $line = fgets($file);

            if ($data['_type'] != 'PriceItem' || !isset($products[$data['barcode']]))
                continue;

            $product = $products[$data['barcode']];

            if (!isset($data['basePrice'])){
                $data['basePrice'] = $data['currentPrice'];
            }

            $found = false;
            foreach ($product as $num => &$price){
                if (!empty($price['chain_id']) && $price['chain_id'] == $this->chain($data['chain'])){
                    if (!isset($price['updated']) && $price['updated_at'] < $today)
                    {
                        $comment = null;
                        if (isset($data['comment'])) $comment = $data['comment'];

                        $prices_insert [] = [$product[0]['id'], $price['chain_id'], $data['basePrice'], $data['currentPrice'], $comment, date('Y-m-d 00:00:00', $today), 0];
                        $products[$data['barcode']][$num]['updated'] = 1;
                        $updated[$product[0]['id']][$price['chain_id']] = 1;
                        //echo Json::encode([$product[0]['id'], $price['chain_id'], $data['basePrice'], $data['currentPrice'], $comment, date('Y-m-d 00:00:00', $today), 0])."\n";
                    }
                    $found = true;
                }
            }
            if (!$found)
            {
                $comment = null;
                if (isset($data['comment'])) $comment = $data['comment'];

                $prices_insert [] = [$product[0]['id'], $this->chain($data['chain']), $data['basePrice'], $data['currentPrice'], $comment, date('Y-m-d 00:00:00', $today), 0];
                $products[$data['barcode']] []= ['chain_id' => $this->chain($data['chain']), 'updated' => 1];
                $updated[$product[0]['id']][$this->chain($data['chain'])] = 1;
                //echo Json::encode([$product[0]['id'], $this->chain($data['chain']), $data['basePrice'], $data['currentPrice']])."\n";
            }


        }
        fclose($file);


        foreach ($products as &$product){
            foreach ($product as &$price){
                if (!isset($updated[$product[0]['id']]) || !isset($updated[$product[0]['id']][$price['chain_id']])){
                    $prices_insert [] = [$price['id'], $price['chain_id'], $price['basePrice'], $price['currentPrice'], $price['comment'], $price['updated_at'], $price['sale_before']];
                    $updated[$product[0]['id']][$price['chain_id']] = 1;
                    //echo Json::encode( [$price['id'], $price['chain_id'], $price['basePrice'], $price['currentPrice'], $price['comment'], $price['updated_at'], $price['sale_before']])."\n";
                }
            }
        }


        if (count($prices_insert) == 0){
            echo "No prices for update\n";
            return "OK";
        }

        Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
        Yii::$app->db->createCommand('DELETE FROM price_tmp WHERE 1')->execute();
        echo "Table 'price_tmp' truncated\n";

        $tr = Yii::$app->db->beginTransaction();
        try
        {
            $len = count($prices_insert);

            $command = Yii::$app->db->createCommand()->batchInsert('price_tmp', ['product_id', 'chain_id', 'basePrice',
                'currentPrice', 'comment', 'updated_at', 'sale_before'], $prices_insert);
            $command->execute();


            $tr->commit();
            echo $len . " prices added\n";
            $tr = Yii::$app->db->beginTransaction();

            Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
            Yii::$app->db->createCommand('RENAME TABLE price_tmp TO tmp, price TO price_tmp, tmp TO price;')->execute();

            $tr->commit();

            echo "Tables were switched\n";
            $m = max($m, memory_get_usage());
            echo $m/1024.0/1024.0 . "mb\n";
        }
        catch(Exception $e){
            $tr->rollBack();
            return ["error" => $e->getMessage(), 'SQL ERROR: updateFromJsonlinesFileByBarcode'];
        }
        return "OK";
    }
}