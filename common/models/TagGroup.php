<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tag_group".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Category[] $categories
 * @property Tag[] $tags
 */
class TagGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('category_tag_group', ['tag_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['group_id' => 'id']);
    }

    public function fields()
    {
        return ['id', 'name'];
    }

    public function extraFields()
    {
        return ['tags'];
    }
}
