<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "barcode".
 *
 * @property integer $product_id
 * @property string $barcode
 *
 * @property Product $product
 */
class Barcode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'barcode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'barcode'], 'required'],
            [['product_id'], 'integer'],
            [['barcode'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'barcode' => 'Barcode',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
