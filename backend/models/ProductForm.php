<?php

namespace backend\models;


use common\models\Category;
use common\models\Product;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

class ProductForm extends Product
{

    private $_categoriesIds;

    public function __construct(Product $product = null){
        parent::__construct();
        if ($product) {
            $this->attributes = $product->attributes;
            $this->id = $product->id;
            $this->isNewRecord = $product->isNewRecord;
        }
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['categoriesIds'], 'safe']
        ]);
    }



    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        //------------------------CATEGORIES----------------
        if ($this->_categoriesIds && count($this->_categoriesIds) > 0){
            if (!$insert)
                $this->unlinkAll('categories', true);

            foreach($this->_categoriesIds as $id)
            {
                $cat = Category::findOne($id);
                $parents = $cat->getParents();
                foreach ($parents as $p) {
                    try {
                        $p->link('products', $this);
                    } catch (Exception $e) {
                    }
                }
                try {
                    $cat->link('products', $this);
                }catch(Exception $e){}
            }
        }
    }

    /**
     * @return mixed
     */
    public function getCategoriesIds()
    {
        if ($this->_categoriesIds === null) {
            $this->_categoriesIds = ArrayHelper::getColumn($this->categories, 'id');
        }
        return $this->_categoriesIds;
    }

    /**
     * @param mixed $categoriesIds
     */
    public function setCategoriesIds($categoriesIds)
    {
        $this->_categoriesIds = $categoriesIds;
    }
}
