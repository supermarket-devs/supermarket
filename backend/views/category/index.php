<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            ['attribute' => 'id', 'options' => ['style' => 'width:80px']],
            [   'attribute' => 'name',
                'format' => 'html',
                'value' => function($model){
                    if ($model->getChildren()->count() > 0)
                        return Html::a($model->name, \yii\helpers\Url::to(['category/', 'CategorySearch[parent]' => $model->id]));
                    return $model->name;
                }
            ],
            [   'attribute' => 'parent',
                'format' => 'html',
                'value' => function($model){
                    $res = '';
                    $parents = $model->parents;
                    foreach ($parents as $i => $p){
                        if ($i > 0)
                            $res .= ' → ';
                        $res .= Html::a($p->name, \yii\helpers\Url::to(['category/', 'CategorySearch[parent]' => $p->id]));
                    }
                    return $res;
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'products' => function ($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-list-alt"></span>',
                            Yii::$app->urlManager->createUrl(['product/index', 'ProductSearch[category]' => $model->id]));
                    }
                ],
                'template' => '{products} {view} {update} {delete}',
            ],
        ],
    ]); ?>

</div>
