<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            ['attribute' => 'id', 'options' => ['style' => 'width:80px']],
            'name',
            'barcode',
//            'description:ntext',
            [
                'attribute' => 'amount',
                'format' => 'html',
                'value' => function($model){
                    return $model->amount . ' '. $model->unit;
                },
                'options' => ['style' => 'width: 70px']
            ],
            [
                'attribute' => 'category',
                'label' => 'Category',
                'format' => 'html',
                'value' => function($model){
                    $res = '';
                    foreach ($model->categories as $cat){
                        if ($cat->getChildren()->count() == 0)
                        {
                            $parents = $cat->parents;
                            foreach ($parents as $i => $p) {
                                if ($i > 0) $res .= ' → ';
                                $res .= Html::a($p->name, Url::to(['product/', 'ProductSearch[category]' => $p->id]));
                            }
                            if (count($parents) > 0) $res .= ' → ';
                            $res .= Html::a($cat->name, Url::to(['product/', 'ProductSearch[category]' => $cat->id]));
                            $res .= '<br>';
                        }
                    }
                    return $res;
                }
            ],
            [
                'attribute' => 'price',
                'label' => 'Prices',
                'format' => 'raw',
                'value' => function($model){
                    $r = '';
                    foreach ($model->prices as $p){
                        $r.= '<div>'.$p->chain->name . ':&nbsp;' .Yii::$app->formatter->asCurrency($p->currentPrice)
                            .'<span class="text-info"> ' . ($p->comment) .' </span></div>';
                    }
                    return $r;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
