<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
        $cats = '';
        foreach ($model->categories as $cat){
            //if ($cat->getChildren()->count() == 0)
            {
                $parents = $cat->parents;
                foreach ($parents as $i => $p) {
                    if ($i > 0) $cats .= ' → ';
                    $cats .= Html::a($p->name, \yii\helpers\Url::to(['product/', 'ProductSearch[category]' => $p->id]));
                }
                if (count($parents) > 0) $cats .= ' → ';
                $cats .= Html::a($cat->name, \yii\helpers\Url::to(['product/', 'ProductSearch[category]' => $cat->id]));
                $cats .= '<br>';
            }
        }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'barcode',
            'description:ntext',
            'amount',
            'unit',
            [   'label' => 'Categories',
                'format' => 'raw',
                'value' => $cats
            ]
        ],
    ]) ?>

    <h3>Prices:</h3>
    <?php
        $attributes = [];
        foreach ($model->prices as $p){
            $a = [
                'format' => 'raw',
                'label' => $p->chain->name,
                'value' => (($p->basePrice != $p->currentPrice)?'<del>'.Yii::$app->formatter->asCurrency($p->basePrice).'</del> ':' ')
                    .Yii::$app->formatter->asCurrency($p->currentPrice) . ' (' . Yii::$app->formatter->asDate($p->updated_at) . ') ' .
                    '<span class="text-info"> ' . ($p->comment) .' </span>'
            ];
            $attributes []= $a;
        }
        echo DetailView::widget([
            'model'=>$model,
            'attributes' => $attributes
        ]);
    ?>

</div>
