<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=supermarket',
            'username' => 'root',
            'password' => '112358',
            'charset' => 'utf8',
            'attributes' => [
                PDO::MYSQL_ATTR_INIT_COMMAND => '
                    SET SESSION sql_mode = \'\';'
            ]
        ]
    ],
];
