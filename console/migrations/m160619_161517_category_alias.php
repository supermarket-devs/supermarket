<?php

use yii\db\Migration;
use yii\db\Schema;

class m160619_161517_category_alias extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('category_alias', [
            'alias' => Schema::TYPE_STRING . ' NOT NULL',
            'category_id' => Schema::TYPE_INTEGER
        ], $tableOptions);

        $this->addPrimaryKey('category_alias_pk', 'category_alias', 'alias');

        $this->execute('INSERT INTO `category_alias`(`alias`, `category_id`) SELECT `url`, `id` FROM `category`');
    }

    public function down()
    {
        $this->dropTable('category_alias');
    }

}
