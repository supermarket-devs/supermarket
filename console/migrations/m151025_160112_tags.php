<?php

use yii\db\Schema;
use yii\db\Migration;

class m151025_160112_tags extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('tag_group', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->createTable('tag', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'group_id' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);

        $this->addForeignKey('fk_tag_group', 'tag', 'group_id', 'tag_group', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('tag');
        $this->dropTable('tag_group');
    }
}
