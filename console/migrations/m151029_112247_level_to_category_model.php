<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_112247_level_to_category_model extends Migration
{
    public function up()
    {
        $this->addColumn('category', 'level', 'smallint(1) NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('category', 'level');
    }
}
