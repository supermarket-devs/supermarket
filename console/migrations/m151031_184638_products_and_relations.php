<?php

use yii\db\Schema;
use yii\db\Migration;

class m151031_184638_products_and_relations extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'barcode' => $this->string(32),
            'description' => $this->text(),
            'amount' => $this->integer(),
            'unit' => $this->string(5),
            'url_novus' => $this->string(),
            'url_mysupermarket' => $this->string(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_category', [
            'product_id' => $this->integer(),
            'category_id' => $this->integer(),
        ], $tableOptions);
        $this->addPrimaryKey('pk_product_category', 'product_category', ['product_id', 'category_id']);
        $this->addForeignKey('fk_product_category_product_id', 'product_category', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_product_category_category_id', 'product_category', 'category_id', 'category', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('product_tag', [
            'product_id' => $this->integer(),
            'tag_id' => $this->integer(),
        ], $tableOptions);

        $this->addPrimaryKey('pk_product_tag', 'product_tag', ['product_id', 'tag_id']);
        $this->addForeignKey('fk_product_tag_product_id', 'product_tag', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_product_tag_tag_id', 'product_tag', 'tag_id', 'tag', 'id', 'CASCADE', 'CASCADE');



        $this->createTable('chain', [
           'id' => $this->primaryKey(),
            'name' => $this->string(16)->notNull()
        ], $tableOptions);

        $this->createTable('price', [
            'product_id' => $this->integer(),
            'chain_id' => $this->integer(),
            'basePrice' => Schema::TYPE_MONEY,
            'currentPrice' => Schema::TYPE_MONEY,
            'comment' => $this->string(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('pk_price', 'price', ['product_id', 'chain_id']);
        $this->addForeignKey('fk_price_product', 'price', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_price_chain', 'price', 'chain_id', 'chain', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('price');
        $this->dropTable('chain');
        $this->dropTable('product_tag');
        $this->dropTable('product_category');
        $this->dropTable('product');
    }

}
