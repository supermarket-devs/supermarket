<?php

use yii\db\Schema;
use yii\db\Migration;

class m151125_202749_amount_fraction extends Migration
{
    public function up()
    {
        $this->alterColumn('product', 'amount', Schema::TYPE_FLOAT);
    }

    public function down()
    {
        $this->alterColumn('product', 'amount', Schema::TYPE_INTEGER);
    }
}
