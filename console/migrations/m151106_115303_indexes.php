<?php

use yii\db\Schema;
use yii\db\Migration;

class m151106_115303_indexes extends Migration
{
    public function up()
    {
        $this->createIndex('ind_product_barcode', 'product', 'barcode');
        $this->createIndex('ind_category_url', 'category', 'url');
    }

    public function down()
    {
        $this->dropIndex('ind_category_url', 'category');
        $this->dropIndex('ind_product_barcode', 'product');
    }
}
