<?php

use yii\db\Schema;
use yii\db\Migration;

class m151117_201509_prices_updated_date_change extends Migration
{
    public function up()
    {
        $this->alterColumn('price', 'updated_at', Schema::TYPE_TIMESTAMP);
        $this->dropColumn('price', 'created_at');
        $this->addColumn('price', 'sale_before', Schema::TYPE_TIMESTAMP . ' DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('price', 'sale_before');
        $this->addColumn('price', 'created_at', Schema::TYPE_INTEGER);
        $this->alterColumn('price', 'updated_at', Schema::TYPE_INTEGER);

    }
}
