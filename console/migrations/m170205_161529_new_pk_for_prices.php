<?php

use yii\db\Migration;

class m170205_161529_new_pk_for_prices extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_price_product', 'price');
        $this->dropPrimaryKey('pk_price', 'price');
        $this->addForeignKey('fk_price_product', 'price', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addColumn('price', 'price_id', \yii\db\Schema::TYPE_BIGPK);
    }

    public function down()
    {
        $this->dropColumn('price', 'price_id');
        $this->addPrimaryKey('pk_price', 'price', ['product_id', 'chain_id']);
    }
}
