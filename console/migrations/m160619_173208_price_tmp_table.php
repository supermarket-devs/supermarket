<?php

use yii\db\Migration;
use yii\db\Schema;

class m160619_173208_price_tmp_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('price_tmp', [
            'product_id' => $this->integer(),
            'chain_id' => $this->integer(),
            'basePrice' => Schema::TYPE_MONEY,
            'currentPrice' => Schema::TYPE_MONEY,
            'comment' => $this->string(),
            'updated_at' => Schema::TYPE_TIMESTAMP,
            'sale_before' => Schema::TYPE_DATE . ' DEFAULT NULL'
        ], $tableOptions);

        $this->addPrimaryKey('pk_price_tmp', 'price_tmp', ['product_id', 'chain_id']);
        $this->addForeignKey('fk_price_tmp_product', 'price_tmp', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_price_tmp_chain', 'price_tmp', 'chain_id', 'chain', 'id', 'CASCADE', 'CASCADE');

        $this->alterColumn('price', 'sale_before', Schema::TYPE_DATE . ' DEFAULT NULL');
    }

    public function down()
    {
        $this->dropTable('price_tmp');
    }
}
