<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m160618_174200_tmp_val_for_product extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'tmp_key', Schema::TYPE_STRING . ' DEFAULT NULL');
    }

    public function down()
    {
        $this->dropColumn('product', 'tmp_key');
    }
}