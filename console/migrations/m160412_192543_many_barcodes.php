<?php

use yii\db\Schema;
use yii\db\Migration;

class m160412_192543_many_barcodes extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('barcode', [
            'product_id' => $this->integer(),
            'barcode' => $this->string(32),
        ], $tableOptions);

        $this->addPrimaryKey('pk_barcode', 'barcode', ['product_id', 'barcode']);

        $this->execute("INSERT INTO `barcode`(product_id, barcode) SELECT id, barcode FROM product");
        $this->dropColumn('product', 'barcode');
    }

    public function down()
    {
        $this->dropTable('barcode');
    }
}
