<?php

use yii\db\Schema;
use yii\db\Migration;

class m151117_203946_product_photo extends Migration
{
    public function up()
    {
        $this->dropColumn('product', 'url_novus');
        $this->dropColumn('product', 'url_mysupermarket');
        $this->dropColumn('product', 'created_at');
        $this->dropColumn('product', 'updated_at');

        $this->addColumn('product', 'photo', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('product', 'photo');

        $this->addColumn('product', 'updated_at', Schema::TYPE_INTEGER);
        $this->addColumn('product', 'created_at', Schema::TYPE_INTEGER);

        $this->addColumn('product', 'url_novus', Schema::TYPE_STRING);
        $this->addColumn('product', 'url_mysupermarket', Schema::TYPE_STRING);
    }
}
