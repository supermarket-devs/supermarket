<?php

use yii\db\Schema;
use yii\db\Migration;

class m151025_161625_categories extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('category', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'parent' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '0'",
        ], $tableOptions);

        $this->createTable('category_tag_group', [
            'category_id' => Schema::TYPE_INTEGER,
            'tag_group_id' => Schema::TYPE_INTEGER
        ], $tableOptions);
        $this->addPrimaryKey('pk_category_tag_group', 'category_tag_group', ['category_id', 'tag_group_id']);

        $this->addForeignKey('fk_categroy_tag_group_category_id', 'category_tag_group', 'category_id', 'category', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_categroy_tag_group_tag_group_id', 'category_tag_group', 'tag_group_id', 'tag_group', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('category_tag_group');
        $this->dropTable('category');
    }

}
