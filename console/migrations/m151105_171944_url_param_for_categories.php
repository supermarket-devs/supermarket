<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_171944_url_param_for_categories extends Migration
{
    public function up()
    {
        $this->addColumn('category', 'url', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('category', 'url');
    }

}
