<?php

namespace console\controllers;

use common\models\ProductUpdater;
use yii\helpers\Json;

class UpdaterController extends \yii\console\Controller
{
    public function actionProducts($filename, $format='json')
    {
        if (!file_exists($filename)) {
            echo "File not exists";
            return 1;
        }
        if ($format == 'json')
            echo Json::encode(ProductUpdater::instance()->updateFromJsonlinesFileByBarcode($filename));
//        elseif ($format == 'csv')
//            echo Json::encode(ProductUpdater::instance()->updateFromCSVFileByBarcode($filename));
        return 0;
    }

    public function actionCategories($filename)
    {
        if (!file_exists($filename)) {
            echo "File not exists";
            return 1;
        }
        echo Json::encode(ProductUpdater::instance()->updateCategoriesFromJsonlinesFile($filename));
        return 0;
    }

}