<?php


namespace app\modules\api\controllers;


use common\models\Chain;
use yii\rest\ActiveController;

class ChainController extends ActiveController
{
    public $modelClass = 'common\models\Chain';

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    public function actionProperty($id, $property){
        return Chain::findOne($id)[$property];
    }
}