<?php

namespace app\modules\api\controllers;


use common\models\TagGroup;
use yii\rest\ActiveController;

class GroupController extends ActiveController
{
    public $modelClass = 'common\models\TagGroup';

    public function actionProperty($id, $property){
        return TagGroup::findOne($id)[$property];
    }
}