<?php

namespace app\modules\api\controllers;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;


use common\models\Price;
use yii\rest\ActiveController;

class PriceController extends ActiveController
{
    public $modelClass = 'common\models\Price';

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create']);
        unset($actions['update']);

        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'only' => ['create', 'update', 'delete'],
            'authMethods' => [
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }

    public function actionProperty($id, $property){
        return Price::findOne($id)[$property];
    }

    public function actionCreate()
    {
        $statusCode = 201;
        $data = Yii::$app->getRequest()->getBodyParams();

        if (!is_array($data))
        {
            $data = [$data];
        }

        $transaction = Yii::$app->db->beginTransaction();
        try
        {
            $models = [];
            foreach ($data as $d) {

                $model = new Price();
                $model->load($d, '');

                if ($model->save()) {
                    $models [] = $model;
                } elseif (!$model->hasErrors()) {
                    throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
                }
            }
            $transaction->commit();
        }
        catch (Exception $e)
        {
            $transaction->rollBack();
            throw $e;
        }

        $response = Yii::$app->getResponse();
        $response->setStatusCode($statusCode);
        return $models;
    }
}