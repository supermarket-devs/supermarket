<?php

namespace app\modules\api\controllers;

use common\models\Category;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class CategoryController extends ActiveController
{
    public $modelClass = 'common\models\Category';

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update']);

        $actions['index']['prepareDataProvider'] = [$this, 'indexDataProvider'];

        return $actions;
    }

    public function indexDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Category::find()->where(['parent' => 0]),
        ]);
    }

    public function actionProperty($id, $property){
        return Category::findOne($id)[$property];
    }
}
