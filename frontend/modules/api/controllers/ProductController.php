<?php

namespace app\modules\api\controllers;

use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;


use common\models\Product;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class ProductController extends ActiveController
{
    public $modelClass = 'common\models\Product';

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'indexDataProvider'];

        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'only' => ['create', 'update', 'delete'],
            'authMethods' => [
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }

    public function indexDataProvider()
    {
        $category = Yii::$app->request->get('category');
        $chains = Yii::$app->request->get('chains');
        $search = Yii::$app->request->get('search');
        $barcode = Yii::$app->request->get('barcode');

        $chains_exploded = null;
        if ($chains) {
            $chains_exploded = explode(',', $chains);
            foreach ($chains_exploded as &$c)
                $c = (int)$c;
        }

        $query = Product::find()->joinWith(['prices' => function ($query) use ($chains_exploded) {
            $query->orderBy('currentPrice')->filterWhere(['in', 'chain_id', $chains_exploded]);
        }])->groupBy('id');

        if ($category)
            $query->joinWith('categories')->where(['category.id' => $category]);

        if ($barcode)
        {
            $query->joinWith('productBarcodes')->where(['like', 'barcode.barcode', $barcode]);
        }

        if ($search)
        {
            $query->andFilterWhere(['like', 'product.name', $search]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['price' => SORT_ASC],
                'attributes' => [
                    'id',
                    'name',
                    'amount',
                    'price' => [
                        'asc' => ['MIN(price.currentPrice)' => SORT_ASC],
                        'desc' => ['MIN(price.currentPrice)' => SORT_DESC],
                    ],
                    'compPrice' => [
                        'asc' => ['MIN(price.currentPrice)/amount' => SORT_ASC],
                        'desc' => ['MIN(price.currentPrice)/amount' => SORT_DESC],
                    ],
                ],
            ],
            'pagination' => [
                'validatePage' => false,
            ]
        ]);

        return $dataProvider;
    }

    public function actionProperty($id, $property){
        return Product::findOne($id)[$property];
    }
}