<?php

return [
        'components' => [
            'request' => [
                'class' => 'yii\web\Request',
                'parsers' => [
                    //'*' => 'yii\web\JsonParser',
                    'application/json' => 'yii\web\JsonParser',
                    'application/xml' => 'yii\web\XmlParser',
                ]
            ],
        ],
];